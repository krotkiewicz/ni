FROM ubuntu:14.04

RUN apt-get update -qq && \
    apt-get -qy install python-pip libpq-dev python-dev

ADD requirements.txt /requirements.txt
RUN pip install -r /requirements.txt

ADD . /app
WORKDIR /app

CMD python manage runserver
