import dj_database_url

from ._base import *

DEBUG = False
TEMPLATE_DEBUG = False
ALLOWED_HOSTS = ['.nintra.net']

S3_HOST = None # default

INSTALLED_APPS = INSTALLED_APPS + (
    'djrill',
)

EMAIL_BACKEND = "djrill.mail.backends.djrill.DjrillBackend"


REST_FRAMEWORK['DEFAULT_RENDERER_CLASSES'] = (
	'rest_framework.renderers.JSONRenderer',
)


defaultdb = dj_database_url.config(default=DB_URL)

defaultdb['ENGINE'] = DATABASES['default']['ENGINE']

DATABASES = {
    'default': defaultdb
}
