"""
Django settings for ni project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import sys
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '1drgmlndkatoogr4!p=eav-*v^p_85t&l^6$#sy%&f3djk7*y1'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []

AUTH_USER_MODEL = 'users.User'

# Application definition
TEMPLATE_DIRS = [os.path.join(BASE_DIR, 'templates')]

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

SHARED_APPS = (
    'tenant_schemas',
    'django.contrib.contenttypes',
    'django.contrib.staticfiles',
    'django.contrib.messages',
    'django.contrib.auth',
    'rest_framework',
    'corsheaders',
    'ni.tenants',
    'ni.bootstrap',
    'ni.global',
)

TENANT_APPS = (
    'django.contrib.admin',
    'django.contrib.sessions',
    'rest_framework.authtoken',
    'ni.access',
    'ni.authapp',
    'ni.clients',
    'ni.config',
    'ni.presence',
    'ni.times',
    'ni.users',
)

INSTALLED_APPS = SHARED_APPS + TENANT_APPS

MIDDLEWARE_CLASSES = (
    'tenant_schemas.middleware.TenantMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'ni.utils.middlewares.GlobalMiddleware',
    'ni.utils.middlewares.ExceptionLoggingMiddleware',
)


ROOT_URLCONF = 'ni.urls'

WSGI_APPLICATION = 'ni.wsgi.application'

CORS_ORIGIN_ALLOW_ALL = True


DATABASES = {
    'default': {
        'ENGINE': 'tenant_schemas.postgresql_backend',
        'NAME': 'nintranet',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        'HOST': 'localhost',
        'PORT': '',
    }
}

DATABASE_ROUTERS = (
    'tenant_schemas.routers.TenantSyncRouter',
)

TENANT_MODEL = 'tenants.Tenant'

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'

APPEND_SLASH = False

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': 'unique-snowflake'
    }
}

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': ('rest_framework.permissions.IsAdminUser',),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.TokenAuthentication',
    ),
    'TEST_REQUEST_DEFAULT_FORMAT': 'json',
    'PAGINATE_BY': 10,
    'MAX_PAGINATE_BY': 100,
    'PAGINATE_BY_PARAM': 'page_size',
}


DEFAULT_FROM_EMAIL = 'noreplay@nintra.net'

from ._s3 import *

DB_URL = None
MANDRILL_API_KEY = None

locals().update(
    (k[3:], v) for k, v in os.environ.iteritems() if k.startswith('NI_')
)

try:
    from .local import *
except ImportError:
    pass


if 'test' in sys.argv:
    ## DO NOT USE TENANTS IN TESTS:
    del DATABASE_ROUTERS
    DATABASES['default']['ENGINE'] = 'django.db.backends.postgresql_psycopg2'
    MIDDLEWARE_CLASSES = tuple(MIDDLEWARE_CLASSES[1:])
    INSTALLED_APPS = [app for app in INSTALLED_APPS if 'tenant' not in app]
    del TENANT_MODEL
    del TENANT_APPS
    del SHARED_APPS
    from ._test import *
