from django.db import models


class Global(models.Model):
    class Meta:
        permissions = (
            ('view_config', "Can view config"),
        )
