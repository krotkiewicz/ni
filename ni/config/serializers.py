from rest_framework import serializers

from .models import Location, Role, OfficeIP


class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Location


class RoleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Role


class OfficeIPSerializer(serializers.ModelSerializer):
    class Meta:
        model = OfficeIP
