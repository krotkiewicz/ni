from django.db import models


class Location(models.Model):
    name = models.CharField(max_length=63, unique=True)

    class Meta:
        permissions = (
            ('get_location', "Can get/list location"),
        )


class Role(models.Model):
    name = models.CharField(max_length=63, unique=True)

    class Meta:
        permissions = (
            ('get_role', "Can get/list role"),
        )


class OfficeIP(models.Model):
    ip = models.CharField(max_length=255, unique=True)
