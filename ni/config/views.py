from rest_framework import viewsets
from ni.authapp.perms import NiDjangoModelPermissions

from .models import Location, Role, OfficeIP
from .serializers import LocationSerializer, RoleSerializer, OfficeIPSerializer


class LocationViewSet(viewsets.ModelViewSet):
    permission_classes = (NiDjangoModelPermissions, )
    queryset = Location.objects.all()
    serializer_class = LocationSerializer
    ordering_fields = ('name',)


class RoleViewSet(viewsets.ModelViewSet):
    permission_classes = (NiDjangoModelPermissions, )
    queryset = Role.objects.all()
    serializer_class = RoleSerializer
    ordering_fields = ('name',)


class OfficeIPViewSet(viewsets.ModelViewSet):
    permission_classes = (NiDjangoModelPermissions, )
    queryset = OfficeIP.objects.all()
    serializer_class = OfficeIPSerializer
