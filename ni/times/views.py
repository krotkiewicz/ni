import datetime

import django_filters
from rest_framework import viewsets, filters

from ni import helpers as h
from ni.authapp.perms import NiOwnerPermissions
from .models import TimeEntry
from .serializers import (
    TimeEntrySerializer, TimeEntryWithProjectSerializer,
)

class DateRangeFilter(django_filters.FilterSet):
    date_start = django_filters.DateFilter(name='date', lookup_type='gte')
    date_end = django_filters.DateFilter(name='date', lookup_type='lte')
    class Meta:
        model = TimeEntry


class TimeEntryViewSet(viewsets.ModelViewSet):
    queryset = TimeEntry.objects.all()
    permission_classes = (NiOwnerPermissions, )
    serializer_class = TimeEntrySerializer
    filter_backends = (filters.OrderingFilter, filters.DjangoFilterBackend)
    filter_class = DateRangeFilter
    ordering_fields = ('date',)
    filter_fields = ('date', 'user', 'deleted')

    def get_serializer_class(self):
        if h.to_bool(self.request.GET.get('withProject')):
            return TimeEntryWithProjectSerializer

        return self.serializer_class

    def perform_update(self, serializer):
        today = datetime.date.today()
        obj = serializer.instance
        data = serializer.validated_data

        past_modification = (
            obj.date < today and
            (
                obj.time != data['time'] or
                obj.project_id != data['project']
            )
        )
        if not past_modification:
            return super(TimeEntryViewSet, self).perform_update(serializer)

        # save the old one:
        obj.deleted = True
        obj.save()

        # force to create new one:
        serializer.instance = None
        serializer.save()

    def perform_destroy(self, instance):
        today = datetime.date.today()
        past_delete = (
            instance.date < today

        )
        if not past_delete:
            super(TimeEntryViewSet, self).perform_destroy(instance)
        else:
            instance.deleted = True
            instance.save()
