from django.db import models
from django.conf import settings
from django.core import validators

from ni.clients.models import Project


class TimeEntry(models.Model):
    time = models.FloatField(
        validators=[
            validators.MinValueValidator(0),
            validators.MaxValueValidator(23.0),
        ],
    )
    description = models.TextField(
        validators=[
            validators.MinLengthValidator(3),
        ],
    )
    date = models.DateField()
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    project = models.ForeignKey(Project)

    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)
    deleted = models.BooleanField(default=False)

    class Meta:
        permissions = (
            ('get_timeentry', "Can get/list time entry"),
            ('manage_own_timeentry', "Can get/create/edit/delete own time entry"),
        )
