from rest_framework import serializers

from ni.clients.serializers import ProjectWithClientSerializer
from ni.clients.models import Project
from ni.users.models import User
from ni.utils.validators import CreateOnlyValidator
from .models import TimeEntry


class TimeEntrySerializer(serializers.ModelSerializer):
    user = serializers.PrimaryKeyRelatedField(
        queryset=User.objects
    )
    project = serializers.PrimaryKeyRelatedField(
        queryset=Project.objects
    )
    created_at = serializers.DateTimeField(read_only=True)
    modified_at = serializers.DateTimeField(read_only=True)
    deleted = serializers.BooleanField(read_only=True)
    date = serializers.DateField(validators=[CreateOnlyValidator])

    class Meta:
        model = TimeEntry
        fields = (
            'id', 'time', 'date', 'user', 'project',
            'created_at', 'modified_at', 'deleted',
            'description',
        )


class TimeEntryWithProjectSerializer(TimeEntrySerializer):
    project = ProjectWithClientSerializer()

