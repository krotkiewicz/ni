import urlparse

from django.template.loader import render_to_string

def to_bool(value, default=False):
    """
       Converts 'something' to boolean. Returns `default` for invalid formats
           Possible True  values: 1, True, '1', 'TRue', 'yes', 'y', 't'
           Possible False values: 0, False, None, [], {}, '', '0', 'faLse', 'no', 'n', 'f', 0.0, ...
    """
    if str(value).lower() in ('yes', 'y', 'true',  't', '1'): return True
    if str(value).lower() in ('no',  'n', 'false', 'f', '0', '0.0', '', 'none', '[]', '{}'): return False
    return default


def getfeurl(request):
    url = request.build_absolute_uri('')
    urlparts = list(urlparse.urlparse(url))
    urlparts[1] = urlparts[1][4:]  # cut api. from domain
    urlparts[2] = ''  # remove path
    url = urlparse.urlunparse(urlparts)
    return url


def render_email(name, ctx):
    subject = render_to_string(
        'subjects/%s' % name,
        ctx,
    )

    subject = ''.join(subject.splitlines())

    message = render_to_string(
        'bodies/%s' % name,
        ctx
    )

    return subject, message
