import uuid

from django.conf import settings
from rest_framework.views import APIView
from rest_framework.parsers import FileUploadParser
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from boto.s3.key import Key

from ni.utils.s3 import get_s3conn


class FileUploadView(APIView):
    permission_classes = (IsAuthenticated, )
    parser_classes = (FileUploadParser,)

    def post(self, request):
        file_ = request.data['file']
        conn = get_s3conn()
        bucket = conn.get_bucket(settings.UPLOAD_BUCKET)
        key = Key(bucket)
        key.key = uuid.uuid4().hex
        # TODO: we shouldn't read file like that:
        key.set_contents_from_string(
            file_.read(),
            headers={'Content-Type': file_.content_type},
            policy="public-read",
        )
        url = key.generate_url(0, query_auth=False)
        return Response(
            data={
                'id': key.key,
                'url': url,
            },
            status=200,
        )
