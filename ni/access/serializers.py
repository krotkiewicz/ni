from django.contrib.auth.models import Group
from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from ni.utils.validators import CreateOnlyValidator
from ni.clients.models import Project, Client
from ni.clients.serializers import ProjectWithClientSerializer, ClientSerializer
from ni.authapp.serializers import GroupSerializer
from ni.users.models import User

from .models import Invitation, DomainAccess


class AccessSerializerMixin(object):
    clients = serializers.PrimaryKeyRelatedField(
        many=True, required=False,
        queryset=Client.objects,
    )
    projects = serializers.PrimaryKeyRelatedField(
        many=True, required=False,
        queryset=Project.objects,
    )
    groups = serializers.PrimaryKeyRelatedField(
        many=True,
        queryset=Group.objects,
    )

    def validate_groups(self, value):
        if not value:
            raise serializers.ValidationError("At least one group is required.")
        return value

    def validate(self, attrs):
        all_and_other = (
            attrs.get('all_clients') and (
                attrs.get('projects') or attrs.get('clients')
            )
        )
        if all_and_other:
            msg = "Provide 'all_clients' or more specific access " \
                  "by providing clients or projects."
            raise serializers.ValidationError(msg)
        return attrs


class InvitationSerializer(AccessSerializerMixin, serializers.ModelSerializer):
    email = serializers.EmailField(
        validators=[
            CreateOnlyValidator(),
            UniqueValidator(
                queryset=Invitation.objects.all(),
                message="Person already invited",
            ),
            UniqueValidator(
                queryset=User.objects.all(),
                message="Person already has account",
            ),
        ]
    )

    class Meta:
        read_only_fields = ('created_at',)
        model = Invitation


class InvitationReadSerializer(InvitationSerializer):
    clients = ClientSerializer(many=True)
    projects = ProjectWithClientSerializer(many=True)
    groups = GroupSerializer(many=True)


class DomainAccessSerializer(AccessSerializerMixin, serializers.ModelSerializer):
    class Meta:
        read_only_fields = ('created_at',)
        model = DomainAccess


class DomainAccessReadSerializer(DomainAccessSerializer):
    clients = ClientSerializer(many=True)
    projects = ProjectWithClientSerializer(many=True)
    groups = GroupSerializer(many=True)
