from django.contrib.auth.models import Group
from django.db import models
from django.core.exceptions import ValidationError
from django.utils import timezone

from ni.clients.models import Client, Project


def domain_validator(value):
    if not '.' in value:
         raise ValidationError("%s is not a valid domain." % value)

class AccessModel(models.Model):
    groups = models.ManyToManyField(Group)
    clients = models.ManyToManyField(Client, default=[])
    projects = models.ManyToManyField(Project, default=[])
    all_clients = models.BooleanField(default=False)


    class Meta:
        abstract = True

    def mark_as_used(self):
        raise NotImplementedError


class NotUsedManager(models.Manager):
    def get_queryset(self):
        return super(NotUsedManager, self).get_queryset().filter(used_at=None)


class Invitation(AccessModel):
    email = models.EmailField(unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    used_at = models.DateTimeField(null=True, blank=True)

    objects = models.Manager()
    notusedobjs = NotUsedManager()

    class Meta:
        permissions = (
            ('get_invitation', "Can get/list invitation"),
        )

    def mark_as_used(self):
        self.used_at = timezone.now()
        self.save()


class DomainAccess(AccessModel):
    domain = models.CharField(
        max_length=127, unique=True, validators=[domain_validator],
    )
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        permissions = (
            ('get_domain_access', "Can get/list domain access"),
        )

    def mark_as_used(self):
        pass



def get_accesses(email):
    try:
        return Invitation.notusedobjs.get(email=email)
    except Invitation.DoesNotExist:
        pass

    domain = email.split('@')[-1]
    try:
        return DomainAccess.objects.get(domain=domain)
    except DomainAccess.DoesNotExist:
        pass
