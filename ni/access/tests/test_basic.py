from model_mommy import mommy
from rest_framework import status
from ni.access.models import Invitation, DomainAccess
from ni.users.models import User

from ni.utils.testing import NiAPITestCase


class AccessTests(NiAPITestCase):
    def test_create_invite(self):
        self.client.force_authenticate(user=self.admin)
        client = self.create_client(name='dupa')
        project = self.create_project(name='dupa', client=client)
        group = self.create_group()

        data = dict(
            email='konrad@o2.pl',
            groups=[group.id],
        )

        response = self.client.post('/api/v1/invitations', data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_invitation(self):
        gs = [mommy.make('Group'), mommy.make('Group')]
        cs = [mommy.make('Client'), mommy.make('Client')]
        ps = [mommy.make('Project'), mommy.make('Project')]

        inv = Invitation.objects.create(
            email='aaa@o2.pl',
        )
        inv.groups.add(*gs)
        inv.clients.add(*cs)
        inv.projects.add(*ps)

        data = {
            'name': 'AAA',
            'email': 'aaa@o2.pl',
            'password': 'aaaaaa',

        }
        self.client.post('/api/v1/signup', data)
        user = User.objects.get(email='aaa@o2.pl')
        self.assertEqual(len(user.groups.all()), 2)
        self.assertEqual(len(user.access.clients.all()), 2)
        self.assertEqual(len(user.access.projects.all()), 2)

        invs = Invitation.notusedobjs.all()
        self.assertEqual(len(invs), 0)

    def test_domain_accesss(self):
        gs = [mommy.make('Group'), mommy.make('Group')]
        cs = [mommy.make('Client'), mommy.make('Client')]
        ps = [mommy.make('Project'), mommy.make('Project')]

        inv = DomainAccess.objects.create(
            domain='dupa.com',
        )
        inv.groups.add(*gs)
        inv.clients.add(*cs)
        inv.projects.add(*ps)

        data = {
            'name': 'AAA',
            'email': 'aaa@dupa.com',
            'password': 'aaaaaa',

        }
        self.client.post('/api/v1/signup', data)
        user = User.objects.get(email='aaa@dupa.com')
        self.assertEqual(len(user.groups.all()), 2)
        self.assertEqual(len(user.access.clients.all()), 2)
        self.assertEqual(len(user.access.projects.all()), 2)

        data = data.copy()
        data['email'] = 'aaa2@dupa.com'
        self.client.post('/api/v1/signup', data)
        user = User.objects.get(email='aaa2@dupa.com')
        self.assertEqual(len(user.groups.all()), 2)
        self.assertEqual(len(user.access.clients.all()), 2)
        self.assertEqual(len(user.access.projects.all()), 2)
