from django.core.mail import send_mail
from rest_framework import viewsets
from rest_framework import filters

from ni import helpers as h
from ni.access.models import Invitation, DomainAccess
from ni.authapp.perms import NiDjangoModelPermissions

from .serializers import (
    InvitationSerializer, InvitationReadSerializer, DomainAccessSerializer,
    DomainAccessReadSerializer,
)


class InvitationsViewSet(viewsets.ModelViewSet):
    queryset = Invitation.notusedobjs.all()
    permission_classes = (NiDjangoModelPermissions, )
    filter_backends = (filters.OrderingFilter,)
    ordering = ('-created_at',)

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return InvitationReadSerializer
        else:
            return InvitationSerializer

    def perform_create(self, serializer):
        super(InvitationsViewSet, self).perform_create(serializer)
        email = serializer.data['email']
        ctx = {
            'request': self.request._request,
            'name': self.request.user.name,
        }
        subject, message = h.render_email('invitation.txt', ctx)
        send_mail(subject, message, None, [email])


class DomainAccessViewSet(viewsets.ModelViewSet):
    queryset = DomainAccess.objects.all()
    permission_classes = (NiDjangoModelPermissions, )
    filter_backends = (filters.OrderingFilter,)
    ordering = ('domain',)

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return DomainAccessReadSerializer
        else:
            return DomainAccessSerializer
