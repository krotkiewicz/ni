from django.contrib.auth.models import Group
from model_mommy import mommy
from rest_framework.test import APITestCase

from ni.bootstrap.management.commands.bootstrap import InitialMixin
from ni.users.models import User


class NiAPITestCase(InitialMixin, APITestCase):

    def create_client(self, **kwargs):
        project = mommy.make('clients.Client', **kwargs)
        return project

    def create_project(self, **kwargs):
        project = mommy.make('clients.Project', **kwargs)
        return project

    def create_group(self, **kwargs):
        group = mommy.make('auth.Group', **kwargs)
        return group

    def setUp(self):
        super(NiAPITestCase, self).setUp()
        self.create_initial_groups()
        employees = Group.objects.get(name='Employees')
        admins = Group.objects.get(name='Admins')

        self.admin = User.objects.create(
            email='admin@o2.pl',
            password='aaaaa',
        )
        admins.user_set.add(self.admin)

        self.user = User.objects.create(
            email='user@o2.pl',
            password='aaaaa',
        )
        employees.user_set.add(self.user)
