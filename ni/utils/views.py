from rest_framework import viewsets


class NiModelViewSet(viewsets.ModelViewSet):
    dropfields = []

    def __add_dropfields(self, kwargs):
        dropfields = set(self.dropfields)
        include = set(self.request.query_params.getlist('include'))
        dropfields = list(dropfields - include)
        kwargs['dropfields'] = dropfields

    def get_serializer_class(self):
        klass = super(NiModelViewSet, self).get_serializer_class()
        if self.request.method == 'GET':
            return klass
        add_dropfields = self.__add_dropfields

        class SerializerClass(klass):
            def __init__(self, *args, **kwargs):
                add_dropfields(kwargs)
                super(SerializerClass, self).__init__(*args, **kwargs)
        return SerializerClass
