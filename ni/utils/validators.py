from django.core import validators
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers

class ForbidenValueValidator(validators.BaseValidator):
    compare = lambda self, a, b: a == b
    message = _('Ensure this value is not %(limit_value)s')
    code = 'forbiden_value'


class CreateOnlyValidator(object):
    def __init__(self):
        self.fieldname = None
        self.is_update = False
        self.old_value = None

    def __call__(self, value):
        if self.is_update and self.old_value != value:
            msg = "%s field cannot be updated." % self.fieldname
            raise serializers.ValidationError(msg)

    def set_context(self, serializer_field):
        self.fieldname = serializer_field.field_name
        instance = serializer_field.parent.instance
        self.is_update = instance is not None
        if self.is_update:
            self.old_value = getattr(instance, self.fieldname)

