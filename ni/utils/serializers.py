from rest_framework import serializers
from rest_framework.fields import Field, empty


class NiJSONField(serializers.Field):
    default_error_messages = {
        'invalid': "Invalid json, has to be json object or array"
    }

    def run_validation(self, data=empty):
        if not isinstance(data, (dict, list)) and data != empty:
            self.fail('invalid')
        return super(NiJSONField, self).run_validation(data)

    def to_internal_value(self, data):
        return data

    def to_representation(self, value):
        return value


class NiModelSerializer(serializers.ModelSerializer):
    def __init__(self, *args, **kwargs):
        dropfields = kwargs.pop('dropfields', [])
        super(NiModelSerializer, self).__init__(*args, **kwargs)

        for dropfield in dropfields:
            self.fields.pop(dropfield)
