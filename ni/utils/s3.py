from boto.s3.connection import S3Connection, OrdinaryCallingFormat
from django.conf import settings


def get_s3conn():
    if settings.S3_HOST:
        kwargs = dict(
            aws_access_key_id='',
            aws_secret_access_key='',
            is_secure=False,
            port=4569,
            host=settings.S3_HOST,
            calling_format=OrdinaryCallingFormat(),
        )
    else:
        kwargs = dict(
            aws_access_key_id=settings.AWS_S3_ACCESS_KEY_ID,
            aws_secret_access_key=settings.AWS_S3_SECRET_ACCESS_KEY,
        )
    s3conn = S3Connection(**kwargs)
    return s3conn
