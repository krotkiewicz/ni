import logging

from ni import helpers as h

class ExceptionLoggingMiddleware(object):

    def process_exception(self, request, exception):
        logging.exception('Exception handling request for ' + request.path)


class GlobalMiddleware(object):

    def process_request(self, request):
        request.feurl = h.getfeurl(request)
