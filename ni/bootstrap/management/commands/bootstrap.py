from django.core.management.base import BaseCommand
from django.contrib.auth.models import Group, Permission
from django.db import connection

from ni.users.models import User


class InitialMixin(object):
    employee_permissions = [
        'clients.get_client',
        'clients.get_project',
        'config.get_location',
        'config.get_role',
        'presence.manage_own_presence',
        'presence.get_presences',
        'times.manage_own_timeentry',
        'times.get_timeentry',
        'users.get_user',
        'users.get_userprofile',
        'users.manage_own_userprofile',
    ]

    owner_permissions = [
         'access.add_domainaccess',
         'access.add_invitation',
         'access.change_domainaccess',
         'access.change_invitation',
         'access.delete_domainaccess',
         'access.delete_invitation',
         'access.get_domain_access',
         'access.get_invitation',

         #'admin.add_logentry',
         #'admin.change_logentry',
         #'admin.delete_logentry',

         #'auth.add_group',
         #'auth.add_permission',
         #'auth.change_group',
         #'auth.change_permission',
         #'auth.delete_group',
         #'auth.delete_permission',

         #'authapp.add_registrationprofile',
         #'authapp.change_registrationprofile',
         #'authapp.delete_registrationprofile',

         #'authtoken.add_token',
         #'authtoken.change_token',
         #'authtoken.delete_token',

         'clients.add_client',
         'clients.add_project',
         'clients.change_client',
         'clients.change_project',
         'clients.delete_client',
         'clients.delete_project',
         'clients.get_client',
         'clients.get_project',

         'config.add_location',
         'config.add_officeip',
         'config.add_role',
         'config.change_location',
         'config.change_officeip',
         'config.change_role',
         'config.delete_location',
         'config.delete_officeip',
         'config.delete_role',
         'config.get_location',
         'config.get_role',

         #'contenttypes.add_contenttype',
         #'contenttypes.change_contenttype',
         #'contenttypes.delete_contenttype',

         #'corsheaders.add_corsmodel',
         #'corsheaders.change_corsmodel',
         #'corsheaders.delete_corsmodel',

         #'global.add_global',
         #'global.change_global',
         #'global.delete_global',
         'global.view_config',

         #'presence.add_presence',
         #'presence.change_presence',
         #'presence.delete_presence',
         'presence.get_presences',
         'presence.manage_own_presence',

         #'sessions.add_session',
         #'sessions.change_session',
         #'sessions.delete_session',

         #'tenants.add_tenant',
         #'tenants.change_tenant',
         #'tenants.delete_tenant',

         'times.add_timeentry',
         'times.change_timeentry',
         'times.delete_timeentry',
         'times.get_timeentry',
         'times.manage_own_timeentry',

         #'users.add_accessprofile',
         'users.add_to_group',
         #'users.add_user',
         #'users.add_userprofile',
         'users.change_accessprofile',
         'users.change_user',
         'users.change_userprofile',
         #'users.delete_accessprofile',
         #'users.delete_user',
         #'users.delete_userprofile',
         'users.get_user',
         'users.get_userprofile',
         'users.get_accessprofile',
         'users.manage_own_userprofile',
    ]


    def create_initial_groups(self):
        employees_group, _ = Group.objects.get_or_create(name='Employees')
        for perm in self.employee_permissions:
            _, perm = perm.split('.')
            perm = Permission.objects.get(codename=perm)
            employees_group.permissions.add(perm)

        owner_group, _ = Group.objects.get_or_create(name='Admins')
        for perm in self.owner_permissions:
            _, perm = perm.split('.')
            perm = Permission.objects.get(codename=perm)
            employees_group.permissions.add(perm)

    def create_initial_user(self, email, name, password):
        user = User.objects.create(
            email=email,
            name=name,
            is_superuser=True,
            is_staff=True,
        )
        user.set_password(password)
        user.save()
        user.access.all_clients = True
        user.access.save()


class Command(InitialMixin, BaseCommand):
    args = '<domain> <email> <name> <password>'

    def _create_tenant(self, domain):
        from ni.tenants.models import Tenant
        tenant = Tenant(
            domain_url=domain,
            schema_name=domain.replace('.', '_'),
        )
        tenant.save()
        return tenant

    def handle(self, *args, **options):
        if len(args) != 4:
            return 'Provide %s' % self.args

        domain, email, name, password = args
        if '.' not in domain:
            return 'Invalid domain'

        return self.command(domain, email, name, password)

    def command(self, domain, email, name, password):
        connection.set_schema_to_public()
        tenant = self._create_tenant(domain)
        connection.set_tenant(tenant)
        self.create_initial_groups()
        self.create_initial_user(email, name, password)
