from django.contrib.auth.models import Group

from ni.users.models import User

class CreateTestUsers(object):
    def handle(self, *args, **options):
        admin = User(
            name='admin',
            email='admin@example.com',
            is_superuser=True,
            is_staff=True,
        )
        admin.set_password('admin')
        admin.save()

        employee = User(
            name='employee',
            email='employee@example.com',
        )
        employee.set_password('employee')
        employee.save()

        employeesg = Group.objects.get(name='Employees')
        employeesg.user_set.add(employee)


