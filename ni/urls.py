from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin
from rest_framework import routers

from .access.views import InvitationsViewSet, DomainAccessViewSet
from .authapp.views import (
    NiObtainAuthToken, GroupViewSet, SignupView, ActivateView,
)
from .users.views import (
    UserViewSet, CurrentUserView, UserProfileViewSet, AccessProfileViewSet,
)
from .clients.views import ClientViewSet, ProjectViewSet
from .times.views import TimeEntryViewSet
from .upload.views import FileUploadView
from .config.views import LocationViewSet, RoleViewSet, OfficeIPViewSet
from .presence.views import PresenceViewSet

router = routers.DefaultRouter(trailing_slash=False)
router.register(r'users', UserViewSet)
router.register(r'clients', ClientViewSet)
router.register(r'projects', ProjectViewSet)
router.register(r'times', TimeEntryViewSet)
router.register(r'userprofiles', UserProfileViewSet)
router.register(r'accessprofiles', AccessProfileViewSet)

router.register(r'locations', LocationViewSet)
router.register(r'roles', RoleViewSet)
router.register(r'officeips', OfficeIPViewSet)
router.register(r'presences', PresenceViewSet)
router.register(r'groups', GroupViewSet)


router.register(r'invitations', InvitationsViewSet)
router.register(r'domainaccess', DomainAccessViewSet)


urlpatterns = patterns('',
    url(r'^api/v1/users/me', CurrentUserView.as_view()),
    url(r'^api/v1/upload', FileUploadView.as_view()),
    url(r'^api/v1/auth', NiObtainAuthToken.as_view()),
    url(r'^api/v1/signup', SignupView.as_view()),
    url(r'^api/v1/activate', ActivateView.as_view()),
    url(r'^api/v1/', include(router.urls)),

)
if settings.DEBUG:
    urlpatterns += patterns('',
	url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
        url(r'^admin/', include(admin.site.urls)),
    )
