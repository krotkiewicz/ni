import re

from django.utils import timezone
from rest_framework import viewsets, filters, status
from rest_framework.exceptions import MethodNotAllowed
from rest_framework.response import Response

from ni.authapp.perms import NiOwnerPermissions
from .models import Presence
from ni.config.models import OfficeIP
from .serializers import PresenceSerializer


class PresenceViewSet(viewsets.ModelViewSet):
    queryset = Presence.objects.all()
    permission_classes = (NiOwnerPermissions, )
    serializer_class = PresenceSerializer
    filter_backends = (filters.OrderingFilter, filters.DjangoFilterBackend)
    filter_fields = ('date', 'user')

    def _get_client_ip(self):
        request = self.request._request
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[0]
        else:
            ip = request.META.get('REMOTE_ADDR')
        return ip

    def _checkip(self):
        clientip = self._get_client_ip()
        # TODO: should be cached.
        ips = OfficeIP.objects.values_list('ip', flat=True).all()
        for ip in ips:
            try:
                matches = re.match(ip, clientip)
            except Exception as e:
                continue
            if matches:
                return True
        return False

    def create(self, request, *args, **kwargs):
        if self._checkip():
            now = timezone.now()
            qs = Presence.objects.filter(
                user=self.request.user,
                date=now.date(),
                first=False,
            )
            count = qs.update(ts=now)
            if not count:
                Presence.objects.get_or_create(
                    user=self.request.user,
                    date=now.date(),
                    first=True,
                )
                Presence.objects.get_or_create(
                    user=self.request.user,
                    date=now.date(),
                    first=False,
                )

            return Response({}, status=status.HTTP_201_CREATED)
        else:
            return Response({}, status=status.HTTP_200_OK)


    def update(self, request, *args, **kwargs):
        raise MethodNotAllowed('PUT')
