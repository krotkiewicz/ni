from rest_framework import serializers

from ni.users.serializers import UserSerializer
from .models import Presence


class PresenceSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True, dropfields=['groups'])
    ts = serializers.DateTimeField(read_only=True)

    class Meta:
        model = Presence
