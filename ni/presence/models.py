from django.db import models
from django.conf import settings


class Presence(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    ts = models.DateTimeField(auto_now_add=True)
    date = models.DateField(auto_now_add=True)
    first = models.BooleanField(default=True)

    class Meta:
        permissions = (
            ('get_presences', "Can get/list presence"),
            ('manage_own_presence', "Can get/create/edit/delete own presence"),
        )
        unique_together = (('user', 'date', 'first'),)
