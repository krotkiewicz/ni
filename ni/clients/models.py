from django.db import models


class Client(models.Model):
    name = models.CharField(max_length=31, unique=True)

    class Meta:
        permissions = (
            ('get_client', "Can get/list clients"),
        )

class Project(models.Model):
    name = models.CharField(max_length=31)
    active = models.BooleanField(default=True)
    client = models.ForeignKey(Client, related_name='projects')

    class Meta:
        unique_together = (('name', 'client'),)
        permissions = (
            ('get_project', "Can get/list projects"),
        )
