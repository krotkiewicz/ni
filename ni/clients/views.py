from django.db.models import Q
from rest_framework import viewsets, filters

from ni import helpers as h
from ni.authapp.perms import NiDjangoModelPermissions
from .serializers import ClientSerializer, ClientWithProjectsSerializer, ProjectSerializer, ProjectWithClientSerializer
from .models import Client, Project



class ClientViewSet(viewsets.ModelViewSet):
    permission_classes = (NiDjangoModelPermissions, )
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
    filter_backends = (filters.OrderingFilter, filters.SearchFilter)
    ordering_fields = ('name',)
    search_fields = ('name',)

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return ClientWithProjectsSerializer
        return self.serializer_class

    def _apply_access(self, qs):
        access = self.request.user.access
        if access.all_clients:
            return qs

        client_ids = access.clients.values('id')
        project_ids = access.projects.values('id')
        qs = qs.filter(
            Q(id__in=client_ids) | Q(projects__id__in=project_ids)
        )

        return qs

    def get_queryset(self):
        qs = super(ClientViewSet, self).get_queryset()
        qs = self._apply_access(qs)
        return qs


class ProjectViewSet(viewsets.ModelViewSet):
    permission_classes = (NiDjangoModelPermissions, )
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    filter_backends = (filters.OrderingFilter, filters.SearchFilter, filters.DjangoFilterBackend)
    ordering_fields = ('name',)
    search_fields = ('name',)
    filter_fields = ('client', )

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return ProjectWithClientSerializer
        if h.to_bool(self.request.GET.get('include_clients')):
            return ProjectWithClientSerializer

        return self.serializer_class

    def _apply_access(self, qs):
        access = self.request.user.access
        if access.all_clients:
            return qs

        client_ids = access.clients.values('id')
        project_ids = access.projects.values('id')

        qs = qs.filter(
            Q(client__in=client_ids) | Q(id__in=project_ids)
        )
        return qs

    def get_queryset(self):
        qs = super(ProjectViewSet, self).get_queryset()
        qs = self._apply_access(qs)
        return qs
