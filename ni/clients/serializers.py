from rest_framework import serializers

from .models import Client, Project


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = ('id', 'name')


class ProjectSerializer(serializers.ModelSerializer):
    client = serializers.PrimaryKeyRelatedField(
        queryset=Client.objects,
    )
    class Meta:
        model = Project
        fields = ('id', 'name', 'client', 'active' )


class ClientWithProjectsSerializer(ClientSerializer):
    projects = ProjectSerializer(many=True)

    class Meta:
        model = Client
        fields = ('id', 'name', 'projects')



class ProjectWithClientSerializer(ProjectSerializer):
    client = ClientSerializer(read_only=True)
