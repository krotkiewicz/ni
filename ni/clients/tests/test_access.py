from model_mommy import mommy

from ni.utils.testing import NiAPITestCase


class AccessTestCase(NiAPITestCase):
    def test_client(self):
        self.client.force_authenticate(user=self.user)
        client = mommy.make('Client')
        project = mommy.make('Project', client=client)

        resp = self.client.get('/api/v1/clients')
        self.assertEqual(len(resp.data['results']), 0)

        self.user.access.all_clients = True
        self.user.access.save()
        resp = self.client.get('/api/v1/clients')
        self.assertEqual(len(resp.data['results']), 1)
        self.user.access.all_clients = False
        self.user.access.save()

        self.user.access.clients.add(client)
        resp = self.client.get('/api/v1/clients')
        self.assertEqual(len(resp.data['results']), 1)
        self.user.access.clients.remove(client)

        self.user.access.projects.add(project)
        resp = self.client.get('/api/v1/clients')
        self.assertEqual(len(resp.data['results']), 1)

    def test_project(self):
        self.client.force_authenticate(user=self.user)
        client = mommy.make('Client')
        project = mommy.make('Project', client=client)

        resp = self.client.get('/api/v1/projects')
        self.assertEqual(len(resp.data['results']), 0)

        self.user.access.all_clients = True
        self.user.access.save()
        resp = self.client.get('/api/v1/projects')
        self.assertEqual(len(resp.data['results']), 1)
        self.user.access.all_clients = False
        self.user.access.save()

        self.user.access.clients.add(client)
        resp = self.client.get('/api/v1/projects')
        self.assertEqual(len(resp.data['results']), 1)
        self.user.access.clients.remove(client)

        self.user.access.projects.add(project)
        resp = self.client.get('/api/v1/projects')
        self.assertEqual(len(resp.data['results']), 1)
