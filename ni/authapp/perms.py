from rest_framework import permissions
from rest_framework.compat import get_model_name


class NiDjangoModelPermissions(permissions.DjangoModelPermissions):
    """
    Extents DjangoModelPermission with additional
    required read permisson for GET access.
    """
    perms_map = {
        'GET': ['%(app_label)s.get_%(model_name)s'],
        'OPTIONS': [],
        'HEAD': [],
        'POST': ['%(app_label)s.add_%(model_name)s'],
        'PUT': ['%(app_label)s.change_%(model_name)s'],
        'PATCH': ['%(app_label)s.change_%(model_name)s'],
        'DELETE': ['%(app_label)s.delete_%(model_name)s'],
    }


class NiOwnerPermissions(NiDjangoModelPermissions):
    def _get_own_perm(self, view):
        model_cls = getattr(view, 'model', None)
        queryset = getattr(view, 'queryset', None)

        if model_cls is None and queryset is not None:
            model_cls = queryset.model

        kwargs = {
            'app_label': model_cls._meta.app_label,
            'model_name': get_model_name(model_cls)
        }
        return '%(app_label)s.manage_own_%(model_name)s' % kwargs

    def has_object_permission(self, request, view, obj):
        result = super(NiOwnerPermissions, self).has_permission(request, view)
        if result:
            return result

        if not request.user.has_perm(self._get_own_perm(view)):
            return False

        return obj.user_id == request.user.id

    def has_permission(self, request, view):
        result = super(NiOwnerPermissions, self).has_permission(request, view)
        if result:
            return result

        if not request.user.has_perm(self._get_own_perm(view)):
            return False

        if request.method == 'GET':
            user_id = str(request.query_params.get('user', ''))
            return user_id == str(request.user.id)

        if request.method == 'POST':
            user_id = str(request.data.get('user', ''))
            return  user_id == str(request.user.id)

        if request.method in ('PUT', 'DELETE'):
            return True

        return False
