from django.contrib.auth import authenticate
from django.contrib.auth.models import Group
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from ni.users.models import User
from ni.access.models import DomainAccess, Invitation, get_accesses


class AuthTokenSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField()

    def validate(self, attrs):
        email = attrs.get('email')
        password = attrs.get('password')

        if email and password:
            user = authenticate(username=email, password=password)

            if user:
                if not user.is_active:
                    msg = _('User account is disabled.')
                    raise serializers.ValidationError(msg)
                attrs['user'] = user
                return attrs
            else:
                msg = _('Unable to log in with provided credentials.')
                raise serializers.ValidationError(msg)
        else:
            msg = _('Must include "email" and "password"')
            raise serializers.ValidationError(msg)


class GroupSerializer(serializers.ModelSerializer):

    class Meta:
        model = Group
        fields = ('id', 'name')


class SignUpSerializer(serializers.Serializer):
    name = serializers.CharField()
    email = serializers.EmailField(
        validators=[
            UniqueValidator(
                queryset=User.objects.all(),
                message="Email already taken",
            ),
        ]
    )
    password = serializers.CharField(min_length=6)

    def validate_email(self, email):
        access = get_accesses(email)
        if not access:
            msg = "This email is not permitted to access this site."
            raise serializers.ValidationError(msg)

        return email


class ActivationSerializer(serializers.Serializer):
    key = serializers.CharField(
        validators=[
            serializers.RegexValidator(
                regex='^[a-f0-9]{40}$',
                message="Invalid key",
            ),
        ],
    )
