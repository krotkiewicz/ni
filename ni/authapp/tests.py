import mock
from rest_framework import status

from ni.utils.testing import NiAPITestCase
from ni.users.models import User
from ni.access.models import Invitation


class AccessTests(NiAPITestCase):

    @mock.patch('ni.users.models.User.email_user')
    @mock.patch('ni.helpers.render_to_string')
    def setUp(self, render_to_string, _):
        super(AccessTests, self).setUp()

        inv = Invitation.objects.create(
            email='aaa@o2.pl',
        )

        data = {
            'name': 'AAA',
            'email': 'aaa@o2.pl',
            'password': 'aaaaaa',

        }
        self.client.post('/api/v1/signup', data)
        self.key = render_to_string.call_args[0][1]['activation_key']

    def test_wrong_key(self):
        self.client.force_authenticate(user=self.admin)

        data = {
            'key': 'a'*39,
        }

        response = self.client.post('/api/v1/activate', data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        data = {
            'key': self.key[1:] + self.key[:1],  # rotate a little bit
        }

        response = self.client.post('/api/v1/activate', data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_good_key(self):
        data = {
            'key': self.key,
        }

        response = self.client.post('/api/v1/activate', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        user = User.objects.get(email='aaa@o2.pl')
        self.assertTrue(user.is_active)
