from django.contrib.auth.models import Group
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.permissions import DjangoModelPermissions
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from .serializers import (
    AuthTokenSerializer, GroupSerializer, SignUpSerializer,
    ActivationSerializer,
)
from .models import RegistrationProfile



class NiObtainAuthToken(APIView):
    permission_classes = ()
    def post(self, request):
        serializer = AuthTokenSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({'token': token.key})


class GroupViewSet(ModelViewSet):
    queryset = Group.objects.all()
    permission_classes = (DjangoModelPermissions, )
    serializer_class = GroupSerializer


class SignupView(APIView):
    permission_classes = ()

    def post(self, request):
        serializer = SignUpSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        data = serializer.data

        RegistrationProfile.objects.create_inactive_user(
            request._request, **data
        )
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class ActivateView(APIView):
    permission_classes = ()
    def post(self, request):
        serializer =  ActivationSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        key = serializer.data['key']
        user = RegistrationProfile.objects.activate_user(key)

        if user:
            return Response({}, status=200)
        else:
            # act like the key was invalid:
            ActivationSerializer(data={'key': 'aa'}).is_valid(
                raise_exception=True
            )
            return Response({}, status=400)

