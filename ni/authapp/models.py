import hashlib
import random
import re

from django.conf import settings
from django.db import models
from django.db import transaction
from django.utils.translation import ugettext_lazy as _

from ni.users.models import User
from ni.access.models import get_accesses
from ni import helpers as h

SHA1_RE = re.compile('^[a-f0-9]{40}$')


class RegistrationManager(models.Manager):
    def activate_user(self, activation_key):
        if SHA1_RE.search(activation_key):
            try:
                profile = self.get(activation_key=activation_key)
            except self.model.DoesNotExist:
                return None
            else:
                user = profile.user
                user.is_active = True
                user.save()
                profile.delete()
                return user
        return None

    @transaction.atomic
    def create_inactive_user(self, request, **kwargs):
        new_user = User.objects.create_user(**kwargs)
        new_user.is_active = False
        new_user.save()

        accessobj = get_accesses(new_user.email)
        new_user.groups.add(*accessobj.groups.all())
        new_user.access.all_clients = accessobj.all_clients
        new_user.access.clients.add(*accessobj.clients.all())
        new_user.access.projects.add(*accessobj.projects.all())
        accessobj.mark_as_used()

        registration_profile = self.create_profile(new_user)

        registration_profile.send_activation_email(request)

        return new_user

    def create_profile(self, user):
        salt = hashlib.sha1(str(random.random())).hexdigest()[:5]
        email = user.email
        if isinstance(email, unicode):
            email = email.encode('utf-8')
        activation_key = hashlib.sha1(salt+email).hexdigest()

        return self.create(
            user=user,
            activation_key=activation_key
        )

class RegistrationProfile(models.Model):

    user = models.ForeignKey(User, unique=True, verbose_name=_('user'))
    activation_key = models.CharField(_('activation key'), max_length=40)

    objects = RegistrationManager()

    def send_activation_email(self, request):
        ctx_dict = {
            'activation_key': self.activation_key,
            'request': request
        }

        subject, message = h.render_email('activation.txt', ctx_dict)

        self.user.email_user(subject, message, settings.DEFAULT_FROM_EMAIL)
