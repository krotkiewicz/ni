from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver

from .models import UserProfile, AccessProfile


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_profiles(sender, created=False, instance=None, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)
        AccessProfile.objects.create(user=instance)

