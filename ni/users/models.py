from django.core.mail import send_mail
from django.db import models
from django.conf import settings
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import (
    AbstractBaseUser, PermissionsMixin, BaseUserManager,
)
from django.core.cache import cache
from jsonfield import JSONField

from ni.clients.models import Client, Project
from ni.utils.s3 import get_s3conn
from ni.utils.validators import ForbidenValueValidator


class UserManager(BaseUserManager):

    def _create_user(self, email, password,
                     is_staff, is_superuser, **extra_fields):
        """
        Creates and saves a User with the given username, email and password.
        """
        now = timezone.now()
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email,
            is_staff=is_staff, is_active=True,
            is_superuser=is_superuser, last_login=now,
            date_joined=now, **extra_fields
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self,  email=None, password=None, **extra_fields):
        return self._create_user(email, password, False, False,
            **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        return self._create_user(email, password, True, True,
            **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    name = models.CharField(_('name'), max_length=63)
    email = models.EmailField(_('email address'), unique=True)
    is_staff = models.BooleanField(_('staff status'), default=False)
    is_active = models.BooleanField(_('active'), default=True)
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    USERNAME_FIELD = 'email'

    objects = UserManager()

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')
        permissions = (
            ('get_user', "Can get/list users"),
            ('add_to_group', "Can add user to group"),
        )

    def get_short_name(self):
        return self.name

    def get_full_name(self):
        return self.name

    @property
    def avatar_url(self):
        return self.profile.avatar_url

    def email_user(self, subject, message, from_email=None, **kwargs):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email], **kwargs)


class AccessProfile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, related_name='access')

    clients = models.ManyToManyField(Client)
    projects = models.ManyToManyField(Project)
    all_clients = models.BooleanField(default=False)

    class Meta:
        permissions = (
            (
                'get_accessprofile',
                "Can get/list access profile",
            ),
        )


class UserProfile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, related_name='profile')
    avatar = models.CharField(
        max_length=63, blank=True, null=True, default=None,
        validators=[
            ForbidenValueValidator(''),
        ],
    )
    location = models.ForeignKey(
        'config.Location', null=True, blank=True,
    )
    roles = models.ManyToManyField('config.Role')

    start_work = models.DateField(null=True, blank=True)
    start_full_time_work = models.DateField(null=True, blank=True)
    birthdate = models.DateField(null=True, blank=True)

    # frontend can put here anything it wants:
    # tel no, skype id, etc
    info = JSONField(default={})

    @property
    def avatar_url(self):
        if not self.avatar:
            return None

        url = cache.get(self.avatar)
        if url:
            return url

        conn = get_s3conn()
        bucket = conn.get_bucket(settings.UPLOAD_BUCKET)
        key = bucket.get_key(self.avatar)
        url = key.generate_url(3600)
        cache.set(self.avatar, url, 3600)
        return url

    class Meta:
        permissions = (
            (
                'get_userprofile',
                "Can get/list user profile"
            ),
            (
                'manage_own_userprofile',
                "Can get/edit own employee profile"
            ),
        )
