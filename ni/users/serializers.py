from django.contrib.auth.models import Group
from rest_framework import serializers, exceptions

from ni.authapp.serializers import GroupSerializer
from ni.clients.models import Client, Project
from ni.clients.serializers import ClientSerializer, ProjectWithClientSerializer
from ni.utils.serializers import NiJSONField, NiModelSerializer
from ni.config.serializers import RoleSerializer, LocationSerializer
from ni.config.models import Location, Role
from .models import User, UserProfile, AccessProfile


class UserProfileWriteSerializer(serializers.ModelSerializer):
    location = serializers.PrimaryKeyRelatedField(
        allow_null=True, queryset=Location.objects,
    )
    roles = serializers.PrimaryKeyRelatedField(
        many=True, queryset=Role.objects,
    )
    info = NiJSONField()

    class Meta:
        model = UserProfile
        fields = (
            'id', 'avatar', 'avatar_url', 'location', 'start_work',
            'start_full_time_work', 'birthdate', 'info',
            'location', 'roles',
        )


class UserProfileReadSerializer(UserProfileWriteSerializer):
    location = LocationSerializer()
    roles = RoleSerializer(many=True)


class AccessProfileWriteSerializer(serializers.ModelSerializer):
    clients = serializers.PrimaryKeyRelatedField(
        many=True, queryset=Client.objects,
    )
    projects = serializers.PrimaryKeyRelatedField(
        many=True, queryset=Project.objects,
    )

    class Meta:
        model = AccessProfile
        fields = ('id', 'clients', 'projects', 'all_clients')


class AccessProfileReadSerializer(AccessProfileWriteSerializer):
    clients = ClientSerializer(many=True)
    projects = ProjectWithClientSerializer(many=True)


class UserSerializer(NiModelSerializer):
    profile = UserProfileReadSerializer()
    groups = GroupSerializer(many=True)

    class Meta:
        model = User
        fields = ('id', 'name', 'email', 'avatar_url', 'profile', 'groups')


class UserWriteSerializer(serializers.ModelSerializer):
    groups = serializers.PrimaryKeyRelatedField(
        many=True, queryset=Group.objects,
    )

    def validate_groups(self, groups):
        user = self.context['request'].user
        if not user.has_perm('users.add_to_group'):
            msg = "Not allowed to edit groups"
            raise exceptions.PermissionDenied(msg)
        return groups

    class Meta:
        model = User
        fields = ('name', 'groups')

