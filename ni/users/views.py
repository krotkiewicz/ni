from rest_framework import viewsets, filters
from rest_framework.exceptions import MethodNotAllowed
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from ni.authapp.perms import NiDjangoModelPermissions, NiOwnerPermissions
from ni.utils.views import NiModelViewSet
from .serializers import (
    UserSerializer, UserWriteSerializer,
    UserProfileReadSerializer, UserProfileWriteSerializer,
    AccessProfileReadSerializer, AccessProfileWriteSerializer,
)
from .models import User, UserProfile, AccessProfile


class UserViewSet(NiModelViewSet):
    permission_classes = (NiDjangoModelPermissions, )
    queryset = User.objects.all()
    serializer_class = UserSerializer
    filter_backends = (filters.OrderingFilter, filters.SearchFilter)
    ordering_fields = ('name', 'email')
    search_fields = ('name', 'email')
    dropfields = ['profile', 'groups']

    def create(self, request, *args, **kwargs):
        raise MethodNotAllowed('POST')

    def get_serializer_class(self):
        if self.action == 'partial_update':
            return UserWriteSerializer
        return super(UserViewSet, self).get_serializer_class()

    def update(self, request, *args, **kwargs):
        if self.action != 'partial_update':
            raise MethodNotAllowed('PUT')

        return super(UserViewSet, self).update(
            request, *args, **kwargs
        )


class CurrentUserView(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        serializer = UserSerializer(
            request.user,
            context=dict(request=request),
        )
        data = serializer.data
        permissions = [
            {'codename': perm}
            for perm in self.request.user.get_all_permissions()
        ]
        data['permissions'] = permissions
        return Response(data)


class UserProfileViewSet(viewsets.ModelViewSet):
    permission_classes = (NiOwnerPermissions, )
    queryset = UserProfile.objects.all()
    serializer_class = UserProfileReadSerializer

    def retrieve(self, request, *args, **kwargs):
        if kwargs.get('pk') == 'me':
            kwargs['pk'] = self.request.user.id
            self.kwargs['pk'] = self.request.user.id
        return super(UserProfileViewSet, self).retrieve(
            request, *args, **kwargs
        )

    def get_serializer_class(self):
        if self.action in ('update', 'create'):
            return UserProfileWriteSerializer
        return self.serializer_class


class AccessProfileViewSet(viewsets.ModelViewSet):
    permission_classes = (NiDjangoModelPermissions, )
    queryset = AccessProfile.objects.all()
    serializer_class = AccessProfileReadSerializer

    def retrieve(self, request, *args, **kwargs):
        if kwargs.get('pk') == 'me':
            kwargs['pk'] = self.request.user.id
            self.kwargs['pk'] = self.request.user.id
        return super(AccessProfileViewSet, self).retrieve(
            request, *args, **kwargs
        )

    def get_serializer_class(self):
        if self.action in ('update', 'create'):
            return AccessProfileWriteSerializer
        return self.serializer_class
