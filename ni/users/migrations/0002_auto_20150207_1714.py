# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='accessprofile',
            options={'permissions': (('get_accessprofile', 'Can get/list access profile'),)},
        ),
        migrations.AlterModelOptions(
            name='userprofile',
            options={'permissions': (('get_userprofile', 'Can get/list user profile'), ('manage_own_userprofile', 'Can get/edit own employee profile'))},
        ),
    ]
