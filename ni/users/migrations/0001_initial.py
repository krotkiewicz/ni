# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields
import ni.utils.validators
import django.utils.timezone
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('clients', '__first__'),
        ('config', '__first__'),
        ('auth', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(default=django.utils.timezone.now, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('name', models.CharField(max_length=63, verbose_name='name')),
                ('email', models.EmailField(unique=True, max_length=75, verbose_name='email address')),
                ('is_staff', models.BooleanField(default=False, verbose_name='staff status')),
                ('is_active', models.BooleanField(default=True, verbose_name='active')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
                ('groups', models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Group', blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of his/her group.', verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Permission', blank=True, help_text='Specific permissions for this user.', verbose_name='user permissions')),
            ],
            options={
                'verbose_name': 'user',
                'verbose_name_plural': 'users',
                'permissions': (('get_user', 'Can get/list users'), ('add_to_group', 'Can add user to group')),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='AccessProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('all_clients', models.BooleanField(default=False)),
                ('clients', models.ManyToManyField(to='clients.Client')),
                ('projects', models.ManyToManyField(to='clients.Project')),
                ('user', models.OneToOneField(related_name='access', to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('avatar', models.CharField(default=None, max_length=63, null=True, blank=True, validators=[ni.utils.validators.ForbidenValueValidator(b'')])),
                ('start_work', models.DateField(null=True, blank=True)),
                ('start_full_time_work', models.DateField(null=True, blank=True)),
                ('birthdate', models.DateField(null=True, blank=True)),
                ('info', jsonfield.fields.JSONField(default={})),
                ('location', models.ForeignKey(blank=True, to='config.Location', null=True)),
                ('roles', models.ManyToManyField(to='config.Role')),
                ('user', models.OneToOneField(related_name='profile', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'permissions': (('get_userprofile', 'Can get/list time entry'), ('manage_own_userprofile', 'Can get/create/edit/delete own employee profile')),
            },
            bases=(models.Model,),
        ),
    ]
